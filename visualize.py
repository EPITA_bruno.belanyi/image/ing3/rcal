#!/usr/bin/env python3

import sys

import vtk

def show_brain(tumor_path):
    brain_reader = vtk.vtkMetaImageReader()
    brain_reader.SetFileName(tumor_path)
    brain_reader.Update()

    brain_cubes = vtk.vtkMarchingCubes()
    brain_cubes.SetValue(0, 0.001) # completely arbitrary value
    brain_cubes.ComputeNormalsOn()
    brain_cubes.SetInputConnection(brain_reader.GetOutputPort())

    brain_mapper = vtk.vtkPolyDataMapper()
    brain_mapper.ScalarVisibilityOff()
    brain_mapper.SetInputConnection(brain_cubes.GetOutputPort())

    brain_actor = vtk.vtkActor()
    brain_actor.SetMapper(brain_mapper)

    renderer = vtk.vtkRenderer()
    renderer.AddActor(brain_actor)

    render_window = vtk.vtkRenderWindow()
    render_window.AddRenderer(renderer)

    iren = vtk.vtkRenderWindowInteractor()
    render_window.SetInteractor(iren)

    iren.Start()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: {} MHA_FILE".format(sys.argv[0]))
        exit(1)

    show_brain(sys.argv[1])
