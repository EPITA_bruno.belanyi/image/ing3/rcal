#!/usr/bin/env python3

import sys

import itk

def find_tumor(brain_file, output_file):
    PixelType = itk.ctype('float')
    ImageType = itk.Image[PixelType, 3]

    reader = itk.ImageFileReader[ImageType].New()
    reader.SetFileName(brain_file)

    # shamelessly stolen from ITK TP, ex 5
    diffusion_filter = itk.GradientAnisotropicDiffusionImageFilter[ImageType, ImageType].New(
        Input = reader,
        NumberOfIterations=20,
        TimeStep=0.04,
        ConductanceParameter=3
    )

    connected_threshold = itk.ConnectedThresholdImageFilter[ImageType, ImageType].New()
    connected_threshold.SetInput(diffusion_filter.GetOutput())
    connected_threshold.SetReplaceValue(255)
    connected_threshold.SetLower(1100.0)
    connected_threshold.SetUpper(4200.0)
    connected_threshold.SetSeed((70, 80, 100)) # tumor center identified manually

    itk.imwrite(connected_threshold, output_file)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usage: {} MHA_FILE OUTPUT_FILE".format(sys.argv[0]))
        exit(1)

    find_tumor(sys.argv[1], sys.argv[2])
