# Mini projet RCAL

Bruno Belanyi, Antoine Martin

## Comment utiliser les fichiers

1. Lancer `segmentation.py` pour créer un fichier `.mha` avec uniquement la
   tumeur dedans. Le script prend deux arguments, d'abord le fichier contenant
   tout le cerveau, puis le nom du fichier à produire avec uniquement la tumeur
   à l'intérieur.

2. Lancer `visualize.py` pour afficher la tumeur segmentée résultante. Le script
   prend un argument, le fichier `.mha` à afficher.

## Example

``` sh
python segmentation.py BRATS_HG0015_T1C.mha my_tumor.mha
python visualize.py my_tumor.mha
```
